"use strict";
/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const bitfield = Symbol('bitfieldSymbol');
exports.bitfield = bitfield;
const eventMap = Symbol('eventMapSymbol');
const kObjectMode = 1 << 0;
exports.kObjectMode = kObjectMode;
const kErrorEmitted = 1 << 1;
exports.kErrorEmitted = kErrorEmitted;
const kAutoDestroy = 1 << 2;
exports.kAutoDestroy = kAutoDestroy;
const kEmitClose = 1 << 3;
exports.kEmitClose = kEmitClose;
const kDestroyed = 1 << 4;
exports.kDestroyed = kDestroyed;
const kClosed = 1 << 5;
exports.kClosed = kClosed;
const kCloseEmitted = 1 << 6;
exports.kCloseEmitted = kCloseEmitted;
const kErrored = 1 << 7;
exports.kErrored = kErrored;
const kConstructed = 1 << 8;
exports.kConstructed = kConstructed;
function runNextTick(fn, ...args) {
    Promise.resolve(args).then((args) => fn(...args));
}
exports.runNextTick = runNextTick;
// foundation/ability/ability_runtime/frameworks/js/napi/app/context/context.js
class EventHub {
    constructor() {
        this[_a] = new Map();
    }
    on(event, callback) {
        let res = this[eventMap].get(event);
        if (!res) {
            this[eventMap].set(event, [callback]);
        }
        else if (res.indexOf(callback) === -1) {
            res.push(callback);
        }
    }
    off(event, callback) {
        if (callback) {
            let res = this[eventMap].get(event);
            if (res) {
                let index = res.indexOf(callback);
                if (index > -1) {
                    if (res.length == 1) {
                        this[eventMap].delete(event);
                    }
                    else {
                        for (; index + 1 < res.length; ++index) {
                            res[index] = res[index + 1];
                        }
                        res.pop();
                    }
                }
            }
        }
        else {
            this[eventMap].delete(event);
        }
    }
    emit(event, ...args) {
        let res = this[eventMap].get(event);
        if (res) {
            const len = res.length;
            for (let i = 0; i < len; ++i) {
                res[i].apply(this, args);
            }
        }
        else if (event === 'error') {
            if (args && args.length > 0) {
                let error = args[0];
                if (error instanceof Error) {
                    throw error;
                }
            }
        }
    }
    listenerCount(event) {
        let res = this[eventMap].get(event);
        if (!res) {
            return 0;
        }
        return res.length;
    }
}
_a = eventMap;
class Stream extends EventHub {
    constructor() {
        super();
    }
}
exports.Stream = Stream;
